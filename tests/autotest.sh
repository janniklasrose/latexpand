#!/bin/bash

# We need /bin/bash, not just /bin/sh, for the syntax
# ENV_VARIABLE=value function

# Set to non-empty for interactive test (show both PDFs)
interactive=
T=
COMPILE_CMD=
ALLOW_WARN=

DIR=$(cd "$(dirname "$0")" && pwd)
PATH="$DIR"/..:"$PATH"

die () {
    printf "%s\\n" "$@"
    exit 1
}

cd "$DIR" || die "Could not cd to $DIR"

mkdir -p actual/err expected/err actual/txt expected/txt

test_file () {
    actual="$DIR/actual/$T$1"
    actualtxt="$DIR/actual/txt/$T$1"
    actualerr="$DIR/actual/err/$T$1"
    expected="$DIR/expected/$T$1"
    expectedtxt="$DIR/expected/txt/$T$1"
    expectederr="$DIR/expected/err/$T$1"
    echo latexpand "$@" ">$actual"
    if [ -n "$COMPILE_CMD" ]; then
	echo "Running command in $PWD ..."
	echo "$COMPILE_CMD"
	eval "$COMPILE_CMD" || die "Command $COMPILE_CMD failed (source)"
    fi
    latexpand "$@" >"$actual" 2>"$actualerr" || die "latexpand failed for $*"

    if grep -e '^[^%]*\\input\>' -e '^[^%]*\\include\>' -e '^[^%]*\\import\>' -e '^[^%]*\\lstinputlisting\>' "$actual"
    then
	die "Include commands remaining in .tex file."
    fi

    if test -s "$actualerr" && test -z "$ALLOW_WARN"
    then
	cat "$actualerr"
	die "Warning or error emitted."
    fi

    pdflatex -interaction=nonstopmode "$1" > /dev/null || die "Compilation of non-expanded file failed for $*"
    (cd "$DIR/actual" && TEXINPUTS=..:"$TEXINPUTS" pdflatex -interaction=nonstopmode "$actual" > /dev/null) || die "Compilation of expanded file failed for $*"
    pdftotext "${1%.tex}.pdf" "$expectedtxt"
    pdftotext "${actual%.tex}.pdf" "$actualtxt"

    diff -u "$expectedtxt" "$actualtxt" || die "Difference found for $*"

    if test -n "$interactive"
    then
	xpdf "${actual%.tex}.pdf"
	xpdf "${1%.tex}.pdf"
    fi

    if test -e "$expected"
    then
	# Get rid of absolute path.
	sed -i "s@$DIR@.@g" "$actual"
	# Get rid of biblatex specifics depending on version
	sed -i "s@biblatex bbl format version.*@biblatex bbl format version XXX \$@" "$actual"
	grep -v '\true{nocite}' "$actual" > "$actual".tmp
	mv "$actual".tmp "$actual"
	diff -u "$expected" "$actual" || die "Actual expanded did not match expected"
	rm -f "$actual"
    fi
    if test -e "$expectederr"
    then
	# Get rid of absolute path.
	sed -i "s@$DIR@.@g" "$actualerr"
	diff -u "$expectederr" "$actualerr" || die "Actual stderr did not match expected"
	rm -f "$actualerr"
    fi

    T=
}

(
    rm -f ./*.bbl actual/*.bbl expected/*.bbl
    COMPILE_CMD="pdflatex -interaction=nonstopmode biblatex.tex && biber biblatex" \
	       test_file biblatex.tex --biber biblatex.bbl
) || exit 1

(
    # Export just in a subshell to avoid disturbing other tests
    export TEXINPUTS=dir1:.:dir2:
    T=includes-dir1-dot-dir2- test_file list-includes.tex

    export TEXINPUTS=dir1:dir2:
    T=includes-dir1-dir2- test_file list-includes.tex
) || exit 1
T=includes-notexinput- test_file list-includes.tex

T=comments- test_file strip-comments.tex

test_file includer-comment.tex
test_file includer-with-end.tex
ALLOW_WARN=t test_file includer.tex
test_file quoted-names.tex
ALLOW_WARN=t T=empty-comments- test_file includer.tex --empty-comments
ALLOW_WARN=t T=explain- test_file includer.tex --explain
ALLOW_WARN=t T=explain-comments- test_file includer.tex --explain --empty-comments
ALLOW_WARN=t test_file includegraphic.tex --show-graphics
T=keep- test_file comments-and-newlines.tex --keep-comments
T=empty- test_file comments-and-newlines.tex --empty-comments
test_file comments-and-newlines.tex
test_file weird-spaces.tex

(
    cd df-conflict/ || die "Can't cd to df-conflict/"
    test_file a.tex
)

T=usepackage- test_file package-user.tex --expand-usepackage
T=usepackagecomments- test_file package-user.tex --expand-usepackage --keep-comments
test_file package-user.tex

T=keep-comments- test_file text-after-end.tex --keep-comments
test_file text-after-end.tex

test_file makeatletter.tex --makeatletter
# Known failure
! ( test_file makeatletter.tex )

echo 'testing @-commands warning...'
if ! latexpand makeatletter.tex 2>&1 | grep -q 'containing @'
then
    die 'latexpand did not warn on @-command'
fi
if latexpand --makeatletter makeatletter.tex 2>&1 | grep -q 'containing @'
then
    die 'latexpand did warn on @-command even with --makeatletter'
fi
echo 'testing @-commands warning... done'

echo 'Testing subimport support...'
latexpand hello_subimport.tex > actual/hello_subimport-o.tex
diff -u actual/hello_subimport-o.tex expected/hello_subimport.tex ||
    die "subimport failed"

echo 'Testing import support...'
sed "s@PWD@$PWD@" hello_import.tex.in > hello_import.tex
sed "s@PWD@$PWD@" expected/hello_import.tex.in > expected/hello_import.tex
latexpand hello_import.tex > actual/hello_import-o.tex
diff -u actual/hello_import-o.tex expected/hello_import.tex ||
    die "import failed"

echo 'Testing CLI options...'
latexpand includer-comment.tex -o actual/dash-dash-o.tex ||
    die "Option -o unsupported"
diff -u actual/dash-dash-o.tex expected/includer-comment.tex ||
    die "Incorrect output on -o"

latexpand includer-comment.tex --output actual/dash-dash-output.tex ||
    die "Option --output unsupported"
diff -u actual/dash-dash-output.tex expected/includer-comment.tex ||
    die "Incorrect output on --output"

latexpand includer-comment.tex -o - > actual/dash-o-dash.tex ||
    die "Option -o - unsupported"
diff -u actual/dash-o-dash.tex expected/includer-comment.tex ||
    die "Incorrect output on -o"

(
    mkdir -p tmp.$$
    cd tmp.$$ || die "Can't cd into temporary directory"

    latexpand --version >actual 2>err ||
	die "Option --version unsupported"
    if [ -s err ]; then
	cat err
	die "Option --version writes on stderr (see above)"
    fi
    printf 'latexpand version %s.\n' "$(git describe --tags HEAD)" >expected
    diff -u actual expected ||
	die "Incorrect output on --version"

    latexpand -h >actual 2>err ||
	die "Option -h unsupported"
    if [ -s err ]; then
	cat err
	die "Option -h writes on stderr (see above)"
    fi
    if ! head -n 1 actual | grep -q "^Usage:"; then
	die "latexpand -h doesn't start with Usage:"
    fi
    latexpand --help >actual 2>err ||
	die "Option --help unsupported"
    if [ -s err ]; then
	cat err
	die "Option --help writes on stderr (see above)"
    fi
    if ! head -n 1 actual | grep -q "NAME"; then
	die "latexpand --help doesn't start with NAME"
    fi
) || exit

rm -fr tmp.$$

echo 'Testing CLI options... done'
